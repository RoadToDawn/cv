Heaps are a simple and elegant data structure for efficiently supporting the priority queue operations insert and extract-min. 
They work by maintaining a partial order on the set of elements which is weaker than the sorted order (so it can be efficient to maintain) yet stronger than random order (so the minimum element can be quickly identified).

A heap-labeled tree is defined to be a binary tree such that the key labeling of each node dominates the key labeling of each of its children.

The heap is a slick data structure that enables us to represent binary trees without using any pointers. 
We store data as an array of keys, and use the position of the keys to implicitly satisfy the role of the pointers. 
We will store the root of the tree in the first position of the array, and its left and right children in the second and third positions, respectively.

Space efficiency thus demands that we not allow holes in our tree i.e. that each level be packed as much as it can be. 
If so, only the last level may be incomplete. By packing the elements of the last level as far to the left as possible, we can represent an n-key tree using exactly n elements of the array.

This implicit representation of binary trees saves memory, but is less flexible than using pointers.
	We cannot store arbitrary tree topologies without wasting large amounts of space. 
	We cannot move subtrees around by just changing a single pointer, only by explicitly moving each of the elements in the subtree. 
This loss of flexibility explains why we cannot use this idea to represent binary search trees, but it works just fine for heaps.

Heaps can be constructed incrementally, by inserting each new element into the left-most open spot in the array, namely the (n+1)st position of a previously n-element heap. 
	This ensures the desired balanced shape of the heap-labeled tree, but does not necessarily maintain the dominance ordering of the keys. 
	The new key might be less than its parent in a min-heap, or greater than its parent in a max-heap.

The solution is to swap any such dissatisfied element with its parent. 
	The new element is now happier, but may still dominate its new parent. 
	We now recur at a higher level, bubbling up the new key to its proper position in the hierarchy. 
	Since we replace the root of a subtree by a larger one at each step, we preserve the heap order elsewhere.

This swap process takes constant time at each level. Since the height of an n-element element heap is log n, each insertion takes at most O(log n) time. 
Thus an initial heap of n elements can be constructed in O(n log n) time through n such insertions

The remaining priority queue operations are identifying and deleting the dominant element. 

Identification is easy, since the top of the heap sits in the first position of the array. Removing the top element leaves a hole in the array. 

This can be filled by moving the element from the right-most leaf (sitting in the nth position of the array) into the first position. 
	The shape of the tree has been restored but the labeling of the root may no longer satisfy the heap property. 
	The root of this mini-heap should be the smallest of three elements, namely the current root and its two children. 
	If the current root is dominant, the heap order has been restored. 
	If not, the dominant child should be swapped with the root and the problem pushed down to the next level. 

This dissatisfied element bubbles down the heap until it dominates all its children, perhaps by becoming a leaf node and ceasing to have any. 
This percolate-down operation is also called heapify, because it merges two heaps (the subtrees below the original root) with a new key.

We will reach a leaf after [lgn] bubble-down steps, each constant time. 
Thus root deletion is completed in O(logn) time. 
Exchanging the maximum element with the last element and calling heapify repeatedly gives an O(nlogn) sorting algorithm, named Heapsort.

You can optimise construction by generating the tree without sorting it and using bubble down on any node with children.  
Although both bubble down and bubble up are O(n log n), because n is the height of the heap we're sorting and bubble down is not sorting the full heap, the sort converges to linear time.
