Single Purpose Principle - Decorator pattern for briefcase content.
Open Closed Principle - Client Listeners for websockets, tcp sockets, different socket libraries.
Liskov Substitution Principle - Unit test architecture for Server Connection Listeners.
Interface Segregation Principle - Logging util - Log writer and log printer.
Dependency Inversion Principle - Stubs 

Abstraction - Interfaces and Abstract classes
Polymorphism - Composition over Inheritance
Inheritance - Two types, Type inheritance and Implementation Inheritance.  Type inheritance should be used only by interfaces.  Implementation interface is for abstract or base classes.
	Implementation intheritance should be no more than three levels deep.
Encapsulation - The logical grouping of associated tasks to make building and maintaining complex system possible by removing the need to have understanding of the entire system in memory when making changes.  The best thing about OO

Aspect-orientated programming